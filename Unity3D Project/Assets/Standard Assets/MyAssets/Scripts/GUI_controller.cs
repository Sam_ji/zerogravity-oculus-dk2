﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GUI_controller : MonoBehaviour {
	private bool pause = false;
	public GameObject maincanvas;
	public GameObject deathcanvas;
	public GameObject victorycanvas;
	private bool death = false;
	private bool victory = false;
	// Update is called once per frame
	
	void Update () {
		if(!death && !victory){
			if(Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.JoystickButton7)){
				pause = !pause;
				if(pause){
					GameObject.FindGameObjectWithTag("Player").GetComponent<Submarine_controller>().enabled = false;
					Time.timeScale = 0.0f;
					maincanvas.SetActive(true);
					AudioListener.volume = 0.0f;

				}
				else{
					GameObject.FindGameObjectWithTag("Player").GetComponent<Submarine_controller>().enabled = true;
					Time.timeScale = 1.0f;
					maincanvas.SetActive(false);
					AudioListener.volume = 1.0f;
				}
			}
		}
		else{
			if(Input.anyKey){
				Reload_this_level();
			}
		}
	}



	public void Reload_this_level(){
		SceneManager.LoadScene(0, LoadSceneMode.Single);
		Time.timeScale = 1.0f;
		AudioListener.volume = 1.0f;
	}

	public void Death(){
		deathcanvas.SetActive(true);
		GameObject.FindGameObjectWithTag("Player").GetComponent<Submarine_controller>().enabled = false;
		Time.timeScale = 0.0f;
		AudioListener.volume = 0.0f;
		death = true;
	}
	public void Victory(){
		victorycanvas.SetActive(true);
		GameObject.FindGameObjectWithTag("Player").GetComponent<Submarine_controller>().enabled = false;
		Time.timeScale = 0.0f;
		AudioListener.volume = 0.0f;
		victory = true;
	}

}
