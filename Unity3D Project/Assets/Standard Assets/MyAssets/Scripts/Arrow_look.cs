﻿using UnityEngine;
using System.Collections;

public class Arrow_look : MonoBehaviour {
	public Transform target;
	
	// Update is called once per frame
	void Update () {
		var rotation = Quaternion.LookRotation(target.position - transform.position);
		transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 1.0f);
	}
}
