﻿using UnityEngine;
using System.Collections;

public class Lightfader : MonoBehaviour {
	private Light m_light;
	public float light_decay_rate;


	void Start () {

		m_light = GetComponent<Light>();
	}
	

	void FixedUpdate () {
		m_light.intensity -=light_decay_rate;
	}
}
