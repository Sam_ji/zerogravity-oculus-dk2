﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Input_fetcher : MonoBehaviour {

	public List<KeyCode> input_queue;
	//TODO Not dictionary but array 2D. 
	//Should be in ControlSettings 
	public Dictionary<string, KeyCode> keymap;

	// Use this for initialization
	void Start () {
		input_queue = new List<KeyCode>();

	}

	void OnGUI () {


		if(Event.current.isMouse)
			input_queue.Add(KeyCode.Mouse0 + Event.current.button);
		if(Event.current.isKey)
			input_queue.Add(Event.current.keyCode);
	}


}

