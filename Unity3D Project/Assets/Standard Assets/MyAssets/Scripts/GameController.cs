﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

[Serializable]
public class Character {
	public int max_shield = 100;
	public int max_energy = 100;
	public int max_health = 100;

}

//Singleton class for storing player data
public class GameController : MonoBehaviour {

	public static GameController m_gamecontroller;
	public Character m_character;

	// Use this for initialization
	void Awake () {
		if(m_gamecontroller == null){
			DontDestroyOnLoad(gameObject);
			m_gamecontroller = this;
		}
		
		else if(m_gamecontroller != this)
			Destroy(gameObject);
	}
		
}
