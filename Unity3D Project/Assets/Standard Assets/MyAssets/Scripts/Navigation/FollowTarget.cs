﻿using UnityEngine;
using System.Collections;

public class FollowTarget : MonoBehaviour {
	public GameObject target;
	Vector3 offset;
	// Use this for initialization
	void Start () {
		offset = target.transform.position - transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		//transform.Translate (target.transform.position - offset);
		transform.position = target.transform.position - offset;
		transform.rotation = target.transform.rotation;


	}
}
