﻿using UnityEngine;
using System.Collections;

public class laserdamage : MonoBehaviour {
	public float laser_force = 10.0f;

	void OnParticleCollision(GameObject other){
		if(other.gameObject.tag != "Player" && other.gameObject.GetComponent<Rigidbody>())
			other.GetComponent<Rigidbody>().AddForce(laser_force * transform.forward);

	}
}
