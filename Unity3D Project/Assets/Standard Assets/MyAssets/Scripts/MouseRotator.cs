﻿using UnityEngine;
using System.Collections;

public class MouseRotator : MonoBehaviour {
	private float x;
	private float y;
	public float rotation_speed = 1.0f;
	public float damping = 1.0f;
	public bool joystick = false;
	public bool InvertY = false;


	// Use this for initialization
	void Start () {
		Vector3 angles = transform.eulerAngles;
		x = angles.y;
		y = angles.x;

		if(GetComponent<Rigidbody>())
			GetComponent<Rigidbody>().freezeRotation = true;
	}
	

	void FixedUpdate () {
	

		if(!joystick){
			x += Input.GetAxis("Mouse X") * rotation_speed * 0.02f;
			if(InvertY)
				y -= -Input.GetAxis("Mouse Y") * rotation_speed * 0.02f;
			else
				y -= Input.GetAxis("Mouse Y") * rotation_speed * 0.02f;
		}
		else{
			x += Input.GetAxis("LX") * rotation_speed * 0.02f;
			if(InvertY)
				y -= -Input.GetAxis("LY") * rotation_speed * 0.02f;
			else
				y -= Input.GetAxis("LY") * rotation_speed * 0.02f;
		}



		var n_rotation = Quaternion.Euler(y,x,0);
		transform.rotation =  Quaternion.Slerp(transform.rotation, n_rotation, Time.time * damping);

	}
}
