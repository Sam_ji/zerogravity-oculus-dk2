﻿using UnityEngine;
using System.Collections;

public class Self_rotation : MonoBehaviour {
	public float max_rotating_force = 10.0f;
	public float min_rotating_force = 10.0f;
	public Vector3 rotation_vector;

	

	void Start(){
		rotation_vector.x = Random.Range(min_rotating_force, max_rotating_force);
		rotation_vector.y = Random.Range(min_rotating_force, max_rotating_force);
		rotation_vector.z = Random.Range(min_rotating_force, max_rotating_force);
	}

	void FixedUpdate () {
		transform.Rotate(rotation_vector * Time.deltaTime);
	}
}
