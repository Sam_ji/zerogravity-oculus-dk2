﻿using UnityEngine;
using System.Collections;

public class TorpedoBehaviour : MonoBehaviour {
	public GameObject explosion;

	void OnCollisionEnter(Collision col){
		GameObject.Destroy(gameObject);
		GameObject n_explosion = (GameObject)Instantiate(explosion,transform.position,transform.rotation);
		Destroy(n_explosion,5);
	}
}
