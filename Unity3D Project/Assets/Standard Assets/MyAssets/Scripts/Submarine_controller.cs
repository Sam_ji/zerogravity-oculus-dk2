﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

[Serializable]
public class Engines{
	
	public float thrust_force;
	public float sideshift_force;
	public float brake_force;
	public float max_velocity;
	public float acceleration_time;
	public float friction;
	
}

[Serializable]
public class S_sounds{
	public bool can_play;
	public GameObject Hit_sound;
	public AudioSource sound_engine;
}

[Serializable]
public class S_weapons{
	public GameObject laser_anchor;
	public Transform torpedo_anchor;
	public GameObject torpedo;
	public float torpedo_cd;
	public float torpedo_speed;
	public bool can_torpedo;
}

[Serializable]
public class S_status{
	public int max_energy;
	public int energy;
	
	public int energy_recovery_rate;
	public bool can_recovery = true;
	public bool can_waste_energy = true;
	
	public int max_shield;
	public int shield;
	
	
	public int max_torpedoes;
	public int torpedoes;
	

	
	public Scrollbar energy_bar;
	public Scrollbar shield_bar;
	public Text torpedo_text;
}

public class Submarine_controller : MonoBehaviour {
	public Engines engines;
	public S_sounds s_sounds;
	public S_weapons s_weapons;
	public S_status s_status;
	private float rotation_volume = 0.0f;
	
	
	void Start(){
		s_sounds.can_play = true;
		engines.acceleration_time = 0;
		s_weapons.can_torpedo = true;
		
		if(GameController.m_gamecontroller != null){
			s_status.max_shield = GameController.m_gamecontroller.m_character.max_shield;
			s_status.max_energy = GameController.m_gamecontroller.m_character.max_energy;
			s_status.energy = s_status.max_energy;
			s_status.shield = s_status.max_shield;
		}
		
	}
	
	
	void Update(){
		
		// If jitter appears, change. Get key here, process in fixedupdate
		if(Input.GetKey(KeyCode.W) ||  Input.GetAxis("Triggers")>0){
			Process_input(KeyCode.W);
			engines.acceleration_time = Mathf.Clamp(engines.acceleration_time + Time.deltaTime,0,1.57f);
		}
		else{
			engines.acceleration_time = 0;
		}
		if(Input.GetKey(KeyCode.A) || Input.GetAxis("RX") == -1)
			Process_input(KeyCode.A);
		if(Input.GetKey(KeyCode.S)|| Input.GetAxis("Triggers")<0)
			Process_input(KeyCode.S);
		if(Input.GetKey(KeyCode.D) || Input.GetAxis("RX") == 1)
			Process_input(KeyCode.D);
		if(Input.GetKey(KeyCode.Space) ||Input.GetAxis("RY") == -1)
			Process_input(KeyCode.Space);
		if(Input.GetKey(KeyCode.Z) ||Input.GetAxis("RY") == 1)
			Process_input(KeyCode.Z);
		if(Input.GetKey(KeyCode.Mouse0) || Input.GetKey(KeyCode.JoystickButton0) && s_status.energy>=10){
			if(s_status.can_waste_energy){
				DoEnergy(-10);
				s_status.can_waste_energy = false;
				StartCoroutine(UnlockWasteEnergy());
			}
			s_weapons.laser_anchor.GetComponentInChildren<ParticleSystem>().Play();
			s_weapons.laser_anchor.GetComponentInChildren<AudioSource>().volume = 1;
			if(s_status.energy<10){

				s_weapons.laser_anchor.GetComponentInChildren<ParticleSystem>().Stop();
				s_weapons.laser_anchor.GetComponentInChildren<AudioSource>().volume = 0;
			}
		}
		if(Input.GetKeyUp(KeyCode.Mouse0) || Input.GetKeyUp(KeyCode.JoystickButton0) && s_weapons.laser_anchor.activeSelf){

			s_weapons.laser_anchor.GetComponentInChildren<ParticleSystem>().Stop();
			s_weapons.laser_anchor.GetComponentInChildren<AudioSource>().volume = 0;
		}
		if((Input.GetKey(KeyCode.Mouse1) || Input.GetKey(KeyCode.JoystickButton1)) && s_status.torpedoes>0){
			if(s_weapons.can_torpedo){
				GameObject n_torpedo;
				n_torpedo =(GameObject) Instantiate(s_weapons.torpedo,s_weapons.torpedo_anchor.position,s_weapons.torpedo_anchor.rotation);
				n_torpedo.GetComponent<Rigidbody>().AddForce(n_torpedo.transform.forward * s_weapons.torpedo_speed,ForceMode.Impulse);
				GameObject.Destroy(n_torpedo,6);
				s_weapons.can_torpedo = false;
				StartCoroutine(UnlockTorpedo());
				s_status.torpedoes--;
				s_status.torpedo_text.text = " X " + s_status.torpedoes.ToString();
			}
			
		}
		
		if(Input.GetAxis("LX")!=0 || Input.GetAxis("LY")!=0 || Input.GetAxis("Mouse X")!=0 || Input.GetAxis("Mouse Y")!=0)
			rotation_volume = Mathf.Clamp(rotation_volume + 0.01f,0.0f,1.0f);
		else
			rotation_volume = Mathf.Clamp(rotation_volume - 0.01f,0.0f,1.0f);
		
		
		s_sounds.sound_engine.volume = Mathf.Max(GetComponent<Rigidbody>().velocity.magnitude/engines.max_velocity,rotation_volume);
		
		
		if(s_status.can_recovery){
			s_status.can_recovery = false;
			StartCoroutine(UnlockRecovery());
			DoEnergy(s_status.energy_recovery_rate);
		}
		
		
	}
	
	void FixedUpdate(){
		//We do this here cause the jitter
		Friction();
	}
	
	
	
	public void DoDamage(int damage){
		s_status.shield = Mathf.Clamp(s_status.shield - damage,0,s_status.max_shield);
		s_status.shield_bar.size = 1.0f - s_status.shield/(float)s_status.max_shield;
		if(s_status.shield == 0){
			Application.LoadLevel(Application.loadedLevel);
		}

	}
	
	public void DoEnergy(int n_energy){
		s_status.energy = Mathf.Clamp(s_status.energy + n_energy, 0, s_status.max_energy);
		s_status.energy_bar.size = 1.0f - s_status.energy/(float)s_status.max_energy;
	}

	void OnTriggerEnter(Collider col){
		if(col.gameObject.tag == "Victory")
			Application.LoadLevel(Application.loadedLevel);
	}

	void OnCollisionEnter(Collision col){
		if(col.gameObject.tag != "Bounds"){
			if(s_sounds.can_play){
				s_sounds.can_play = false;
				GameObject hit_sound = Instantiate(s_sounds.Hit_sound);
				ContactPoint contact = col.contacts[0];
				hit_sound.transform.position = contact.point;
				hit_sound.transform.SetParent(transform);
				hit_sound.GetComponent<AudioSource>().volume =col.relativeVelocity.magnitude/engines.max_velocity;
				DestroyObject(hit_sound,1);
				StartCoroutine(UnlockAfter(2.0f));
				DoDamage(Mathf.RoundToInt(col.relativeVelocity.magnitude/engines.max_velocity * 20));
			}
		}
	}
	
	public IEnumerator UnlockWasteEnergy(){
		yield return new WaitForSeconds(0.3f);
		s_status.can_waste_energy = true;
	}
	
	public IEnumerator UnlockRecovery(){
		yield return new WaitForSeconds(2.0f);
		s_status.can_recovery = true;
	}
	
	public IEnumerator UnlockTorpedo(){
		yield return new WaitForSeconds(s_weapons.torpedo_cd);
		s_weapons.can_torpedo = true;
	}
	public IEnumerator UnlockAfter(float delay){
		yield return new WaitForSeconds(delay);
		s_sounds.can_play = true;
	}
	
	void Process_input(KeyCode key){
		
		switch(key){
		case KeyCode.W:
			GetComponent<Rigidbody>().AddForce(transform.forward * engines.thrust_force * Mathf.Sin(engines.acceleration_time), ForceMode.VelocityChange);
			Clip_velocity();
			break;
			
			
		case KeyCode.A:
			GetComponent<Rigidbody>().AddForce(transform.right * (-engines.sideshift_force), ForceMode.VelocityChange);
			Clip_velocity();
			break;
			
		case KeyCode.D:
			GetComponent<Rigidbody>().AddForce(transform.right * engines.sideshift_force, ForceMode.VelocityChange);
			Clip_velocity();
			break;
			
		case KeyCode.Space:
			GetComponent<Rigidbody>().AddForce(transform.up * engines.sideshift_force, ForceMode.VelocityChange);
			Clip_velocity();
			break;
			
		case KeyCode.Z:
			GetComponent<Rigidbody>().AddForce(transform.up * (-engines.sideshift_force), ForceMode.VelocityChange);
			Clip_velocity();
			break;
			
		case KeyCode.S:
			Progressive_Brake();
			Clip_velocity();
			break;
			
		}
	}
	
	void Progressive_Brake(){
		GetComponent<Rigidbody>().AddForce(transform.forward * (-engines.brake_force), ForceMode.VelocityChange);
	}
	
	void Clip_velocity(){
		GetComponent<Rigidbody>().velocity = Vector3.ClampMagnitude(GetComponent<Rigidbody>().velocity,engines.max_velocity);
		
	}
	void Friction(){
		if(GetComponent<Rigidbody>().velocity.magnitude>0.1)
			GetComponent<Rigidbody>().AddForce(GetComponent<Rigidbody>().velocity * -engines.friction, ForceMode.Acceleration);
		else{
			GetComponent<Rigidbody>().velocity = Vector3.zero;
		}
		
	}
}