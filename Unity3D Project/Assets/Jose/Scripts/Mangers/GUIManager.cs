﻿using UnityEngine;
using System.Collections;

public class GUIManager : MonoBehaviour {
	public GUISkin basicSkin;
	public ControlAstrounauta astronauta;
	public Texture redCloud;
	public Rigidbody estacionEspacial;

	public TextMesh texto;
	public TextMesh textoWin;
	
	public GameObject escenaFinal;
	
	public bool inGame;
	bool cameraFollow;
	string textoAPintar = "";
	void Start(){
		inGame = true;
		cameraFollow = false;
		estacionEspacial.AddTorque (estacionEspacial.transform.up*100000000000	);
		
	}

	void Update (){
	//Debug.Log (textoAPintar);
	//PintarTextoEnPantalla("update");
		if (inGame) {
			if (astronauta.calor >= 98) {
					//astronauta.GiroAleatorio(false);
					TerminarPartida (true);
					
					
			}
			
			if (astronauta.oxigeno <= 0)
				TerminarPartida(false);
				
			if (astronauta.ganarPartida)
				GanarPartida ();
		}
		


		
		if (cameraFollow)
			CameraFollow ();
			
		PintarTextoEnPantalla(textoAPintar);
	}

	void OnGUI(){
		GUI.skin = basicSkin;



		Rect rect = new Rect (Screen.width * 0.05f,
		                      Screen.height * 0.10f,
		                      Screen.width * 0.15f,
		                      Screen.height * 0.08f);
		CambiarColor (astronauta.carga);
		GUI.Label (rect, "CARGA:"+string.Format ("{0:0.#}",astronauta.carga)+ "%");


	
		rect = new Rect (Screen.width * 0.05f,
		                 Screen.height * 0.15f,
		                 Screen.width * 0.15f,
		                 Screen.height * 0.18f);
		CambiarColor (astronauta.oxigeno);
		GUI.Label (rect, "OXIGENO:" + string.Format ("{0:0.#}", astronauta.oxigeno) + "%");



		rect = new Rect (Screen.width * 0.05f,
		                 Screen.height * 0.20f,
		                 Screen.width * 0.15f,
		                 Screen.height * 0.50f);
		                 
		CambiarColor (astronauta.altura);
		if (!astronauta.enAtmosfera)
			GUI.Label (rect, "ALTURA:" + string.Format ("{0:0.#}", astronauta.altura));
		else 
			GUI.Label (rect, "ALTURA:" + "DANGER");           
		                 
		                 
		rect = new Rect (0f,
		                 0f,
		                 Screen.width,
		                 Screen.height);

		//para dibujar la textura con alpha = .5
		Color color = new Color ();
		color = Color.white;
		color.a = 1f*astronauta.calor/100;
		if (!inGame) 
			color.a = 0;
		GUI.color = color;
		GUI.DrawTexture (rect, redCloud);

		

	}

 	void GanarPartida(){
 		inGame = false;
 		textoAPintar = "You are alive!!";
 		escenaFinal.SetActive (true);
 		astronauta.gameObject.SetActive (false);
		GameObject.Find ("UI_Objects").SetActive(false);
		//cameraFollow = true;
		//Camera.main.transform.parent = null;
 	}
 	
 	
	void TerminarPartida(bool withStop){
		inGame = false;
		textoAPintar = "Game Over";
		astronauta.GiroAleatorio(withStop);
		astronauta.enabled = false;
		astronauta.Morir ();
		cameraFollow = true;


		GameObject.Find ("UI_Objects").SetActive(false);
		Camera.main.transform.parent = null;
		Debug.Log (textoAPintar);
	}

	void PintarTextoEnPantalla(string s){
	
		texto.text = s;
		textoWin.text = s;
		
	}

    void CameraFollow(){
		Vector3 cameraTarget = new Vector3();
		Vector3 offset =new Vector3 (-12,0,0);
		cameraTarget = astronauta.transform.position;
		cameraTarget+=offset;

		
		Camera.main.transform.position =  Vector3.Slerp (Camera.main.transform.position,cameraTarget ,0.1f);
		Camera.main.transform.LookAt (astronauta.transform.position);
    }
   

	void CambiarColor (float valor){
		if (valor > 50) {
			GUI.color = Color.green;
		}
		else{
			if (valor > 15){
				GUI.color = Color.yellow;
			}else {
				GUI.color = Color.red;
			}
			
		}
	}
	
	

}
