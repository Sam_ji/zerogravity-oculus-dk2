﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GeneralManager : MonoBehaviour {
	//public Collider trigger;
	public GameObject []modeToDisable;
	public GameObject []modeToEnable;
	// Use this for initialization
	public bool modoAstronauta = true;
	public bool modoPMU = false;

	public GameObject []objetosModoAstronauta;
	public GameObject []objetosModoPMU;

	void Start (){
		for (int i = 0; i < objetosModoAstronauta.Length; i++){
			objetosModoAstronauta[i].SetActive (true);
			Debug.Log ("enable object?");
		}
	}


	void Update (){
		if (Input.GetKeyDown (KeyCode.F5)) {
			
			CambioManualDeModo();
		}
	}
	void OnTriggerEnter(Collider Col)
	{	
		Debug.Log ("trigger on " + Col.gameObject.name);
		if (Col.tag =="Player")
			ActivarModoPMU ();	

	}



	void CambioManualDeModo(){
		Debug.Log ("CAMBIO MODO");
		if (modoAstronauta == true){
			modoAstronauta = false;
			modoPMU = true;
			for (int i = 0; i < modeToDisable.Length;i ++){
				modeToDisable[i].SetActive (false);

			}

			for (int i = 0; i < modeToEnable.Length;i ++){
				modeToEnable[i].SetActive (true);

			}
		} else{

		if (modoPMU == true){
			//Application.LoadLevel("scene_race");
				SceneManager.LoadScene ("scene_race");
			/*
			modoAstronauta = true;
			modoPMU = false;
			for (int i = 0; i < objetosModoAstronauta.Length; i++){
				objetosModoAstronauta[i].SetActive (true);
			}

			for (int i = 0; i < objetosModoPMU.Length; i ++){
				objetosModoPMU[i].SetActive (false);
			}
			*/
			}
		}



	}

	void ActivarModoPMU (){
		for (int i = 0; i < modeToDisable.Length;i ++){
			modeToDisable[i].SetActive (false);

		}

		for (int i = 0; i < modeToEnable.Length;i ++){
			modeToEnable[i].SetActive (true);

		}
	}

}
