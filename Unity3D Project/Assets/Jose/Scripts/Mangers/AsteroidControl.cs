﻿using UnityEngine;
using System.Collections;

public class AsteroidControl : MonoBehaviour {

	public GameObject meteoroPrefab;
	public GUIManager gameManager;
	public Transform meteoroTarget;
	public float asteroidSpeed=1;
	public float desvioMax = 0;
	public Transform spawnPoint;
	public Transform spawnPointDummies;
	
	public GUIManager manager;
	
	
	// Use this for initialization
	
	
	void Start () {
		InvokeRepeating ("CrearMeteoro", 2.0f,1.0f);
	//	InvokeRepeating ("CrearMeteoroDummy",2.0f,0.8f) 	;
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void CrearMeteoro(){
		if ( !manager.inGame)
			return;
		Vector3 inicio = transform.position + (Vector3.forward*Random.value) + (Vector3.up*Random.value);
		
		
		GameObject meteoro = Instantiate (meteoroPrefab, inicio, Quaternion.identity) as GameObject;
		
		
		Vector3 desvio = new Vector3 (Random.Range (0, desvioMax),Random.Range (0, desvioMax),Random.Range (0, desvioMax));
		//Vector3 direction = ( (meteoroTarget.position + Random.insideUnitSphere) - inicio).normalized;
		Vector3 direction = ( (meteoroTarget.position+desvio) - inicio).normalized;

		
		Vector3 fuerza = direction * asteroidSpeed;
		//Debug.Log ("V:" + fuerza);
		meteoro.GetComponent<Rigidbody>().AddForce(fuerza, ForceMode.Impulse);
		meteoro.GetComponent<Rigidbody>().AddTorque (new Vector3(Random.value, Random.value, Random.value));
		
	}
	
	void CrearMeteoroDummy(){
		Vector3 inicio = transform.position + (Vector3.forward*Random.value) + (Vector3.up*Random.value);
		
		
		GameObject meteoro = Instantiate (meteoroPrefab, inicio, Quaternion.identity) as GameObject;
		
		
		Vector3 desvio = new Vector3 (Random.Range (3, 4),Random.Range (3, 4),Random.Range (3, desvioMax));
		//Vector3 direction = ( (meteoroTarget.position + Random.insideUnitSphere) - inicio).normalized;
		Vector3 direction = ( (meteoroTarget.position+desvio) - inicio).normalized;
		
		
		Vector3 fuerza = direction * asteroidSpeed;
		//Debug.Log ("V:" + fuerza);
		meteoro.GetComponent<Rigidbody>().AddForce(fuerza, ForceMode.Impulse);
		meteoro.GetComponent<Rigidbody>().AddTorque (new Vector3(Random.value, Random.value, Random.value));
	}
}
