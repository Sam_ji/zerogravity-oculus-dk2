﻿using UnityEngine;
using System.Collections;

public class TimeOut : MonoBehaviour {
	public float lifeTime = 5f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		lifeTime -= Time.deltaTime;
		
		if (lifeTime <= 0)
			Destroy (gameObject);
	}
}
