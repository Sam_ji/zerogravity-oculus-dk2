﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody))]
[RequireComponent (typeof(AudioSource))]

public class ControlAstrounauta : MonoBehaviour {
	float maxCarga;
	[HideInInspector] public float calor = 0f;
	[HideInInspector] public float altura;
	[HideInInspector] public bool enAtmosfera = false;
	[HideInInspector] public bool ganarPartida = false;



	public float fuerzaDesplazamiento = 1f;
	public float fuerzaGiro = 0.06f;
	public float carga = 100f;
	public float oxigeno = 100f;
	public float consumoOxigeno = 2f;
	public float descarga = 0.04f;
	public float fuerzaEstabilizado = 0.6f;
	//public Transform planeta;


	public AudioSource hitSound;
	public AudioSource breathSound;
	bool muerto = false;
	public GameObject muerteGUI;
	// Use this for initialization
	void Start () {
		maxCarga = carga;

	}
	
	// Update is called once per frame
	void Update () {
	
	//faltan los deltatime para los incrementos de temperatura!

		if (enAtmosfera )
				//calor += 0.01f;
			calor += 0.2f;
		else 
			calor -= 0.1f;
				//calor -= 0.005f;

		calor = Mathf.Clamp (calor, 0, 98);

		if (oxigeno > 0) {
			oxigeno -= Time.deltaTime * consumoOxigeno;
		}
		
		if (oxigeno <= 0)
			Morir();


		Vector3 direccion = new Vector3 ();
	//	direccion = planeta.position -transform.position;
		RaycastHit hit;
		Ray rayo = new Ray();
		
		rayo.origin = transform.position;
		rayo.direction = direccion;


		if (muerto == true){
			if (Input.GetKey (KeyCode.KeypadEnter)) {
				Application.LoadLevel ("scene_race");
			}

		}
		//Debug.Log ("O"+rayo.origin);
		//Debug.Log ("D"+rayo.direction);
		
		//Physics.Raycast (transform.position, direccion, out hit, direccion.magnitude);
		//este raycast solo tiene en ecuenta el planeta.getcomponent<Collider>()
		//planeta.GetComponent<Collider>().Raycast (rayo, out hit, direccion.magnitude);
		
//		altura = hit.distance;
//		Debug.Log ("control"+altura);
	}

	void FixedUpdate(){
		if (carga > 0) {
						//acelerar
			if (Input.GetKey (KeyCode.W) ||  Input.GetAxis("Triggers")>0) {
								GetComponent<Rigidbody>().AddForce (transform.forward * fuerzaDesplazamiento);
								carga -= descarga;
						}
						//frenar
			if (Input.GetKey (KeyCode.S) || Input.GetAxis("Triggers")<0) {
								GetComponent<Rigidbody>().AddForce (-transform.forward * fuerzaDesplazamiento);
								carga -= descarga;
						}
			
						//guiñada derecha
			if (Input.GetKey (KeyCode.L) || Input.GetAxis("LX") == 1) {
								GetComponent<Rigidbody>().AddTorque (transform.up * fuerzaGiro);
								carga -= descarga;
						}
						//guiñada izquierda
			if (Input.GetKey (KeyCode.J) || Input.GetAxis("LX") == -1 ) {
								GetComponent<Rigidbody>().AddTorque (-transform.up * fuerzaGiro);
								carga -= descarga;
						}
						////giro sobre eje z
						//alabeo izquierda 
			if (Input.GetKey (KeyCode.A) ||Input.GetAxis("RX") == -1) {
								GetComponent<Rigidbody>().AddTorque (transform.forward * fuerzaGiro);
								carga -= descarga;
						}
						//alabeo derecha
			if (Input.GetKey (KeyCode.D) ||Input.GetAxis("RX") == 1) {
								GetComponent<Rigidbody>().AddTorque (-transform.forward * fuerzaGiro);
								carga -= descarga;
						}
			
						// cabeceo abajo
			if (Input.GetKey (KeyCode.I) ||Input.GetAxis("LY") == 1) {
								GetComponent<Rigidbody>().AddTorque (transform.right * fuerzaGiro);
								carga -= descarga;
						}
						//cabeceo arriba
			if (Input.GetKey (KeyCode.K) ||Input.GetAxis("LY") == -1) {
								GetComponent<Rigidbody>().AddTorque (-transform.right * fuerzaGiro);
								carga -= descarga;
						}	

						//boton del panico
			if (Input.GetKey (KeyCode.Space) || Input.GetKey(KeyCode.JoystickButton0)) {
								GetComponent<Rigidbody>().velocity = Vector3.Lerp (GetComponent<Rigidbody>().velocity, Vector3.zero, Time.fixedDeltaTime * fuerzaEstabilizado);
								//Vector3.Lerp (rigidbody.velocity, Vector3.zero, Time.fixedDeltaTime);
								GetComponent<Rigidbody>().angularVelocity = Vector3.Lerp (GetComponent<Rigidbody>().angularVelocity, Vector3.zero, Time.fixedDeltaTime * fuerzaEstabilizado);
								carga -= descarga;
						}
				} else
						carga = Mathf.Clamp (carga, 0f, maxCarga);
	}

	public void GiroAleatorio(bool withStop){
		Vector3 vectorAleatorio = new Vector3 (Random.value*3f, Random.value*3f, Random.value*3f);

		if (withStop)
			GetComponent<Rigidbody>().velocity = Vector3.zero;
		GetComponent<Rigidbody>().AddTorque (vectorAleatorio);
	}

	void OnTriggerStay(Collider other){
		//if (other.name == "Planeta02Atmosfera") {
		//	calor += 0.01f	;
		}

	void OnTriggerEnter(Collider other){
		//Debug.Log (other.name);
		if (other.name == "Planet02Atmosfera") {
			enAtmosfera = true;
		
		}
		if (other.gameObject.tag == "Acceso"){
			ganarPartida = true;
			Debug.Log ("finpartida");
		}

	}

	void OnTriggerExit(Collider other){
		if (other.name == "Planet02Atmosfera")
			enAtmosfera = false;
	}
	
	
	void OnCollisionEnter (Collision col){
		//print ("Velocidad de colision:" +col.relativeVelocity.magnitude);

	
		//if (col.relativeVelocity > 100)


		hitSound.Play();	
		
	}

	public void Morir(){
		//GetComponent<AudioSource>().Stop();
		breathSound.Stop();
		muerteGUI.SetActive (true);
		muerto= true;
		Time.timeScale = 0;
	}






}




