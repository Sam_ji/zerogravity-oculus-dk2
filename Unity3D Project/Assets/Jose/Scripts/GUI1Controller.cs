﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class GUI1Controller : MonoBehaviour {
	public GameObject canvas1;
	public GameObject firstController;
	public GameObject firstCharacter;

	public void Start (){


		Time.timeScale = 0;
		canvas1.SetActive (true);
		if (firstController != null)
			firstController.SetActive (false);
		firstCharacter.SetActive(true);


	}

	public void Update (){
		if (Input.anyKey){
			canvas1.SetActive (false);
			if (firstController != null)
				firstController.SetActive (true);
			Time.timeScale = 1;
			this.enabled = false;

		}
			
			
	}

}
